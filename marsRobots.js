// Michael Ferro: RedBadger Test

class RobotsOnMars {
  constructor(
	xAxisLength,
	yAxisLength = 1,
	initialX = 1,
	initialY = 1,
	direction = "E"
  ) {
	this.xAxisLength = xAxisLength > 50 ? 50 : xAxisLength;
	this.yAxisLength = yAxisLength > 50 ? 50 : yAxisLength;

	this.initialX = initialX <= this.xAxisLength ? initialX : 1;
	this.initialY = initialY <= this.yAxisLength ? initialY : 1;

	this.direction = direction.toLowerCase().match(/^([nesw])/i) ? direction.toLowerCase() : "e";
	console.log(`${xAxisLength} ${yAxisLength}`);
	console.log(`${this.initialX} ${this.initialY} ${this.direction}`);
  }

  flatten(arr) {
	Array.prototype.concat(...arr);
  }

  buildGrid() {
	this.mars2d = Array(this.yAxisLength)
	  .fill(".")
	  .map(x => Array(this.xAxisLength).fill("."));
	this.mars2d[this.initialX][this.initialY] = "I";

	this.showSurface();
  }

  setMarker(currentCoords, incrementor, limitPos) {
	currentCoords == ".";
	incrementor();
  }

  resolver(actionCount, coords, axis, limitPos, incrementor) {
	if (axis() == limitPos) {
	  return false;
	} else {
	  this.setMarker(
		this.mars2d[coords.yPos][coords.xPos],
		incrementor,
		limitPos
	  );
	  return true;
	}
  }

  resolveActions(actionCount, coords, axis, limitPos, incrementor) {
	let res = true;
	if (incrementor.toString().match(/\+\+/)) {
	  for (let i = 0; i < actionCount && axis() <= limitPos; i++) {
		res = this.resolver(actionCount, coords, axis, limitPos, incrementor);
	  }
	} else {
	  for (let i = 0; i < actionCount && axis() >= limitPos; i++) {
		res = this.resolver(actionCount, coords, axis, limitPos, incrementor);
	  }
	}

	this.mars2d[coords.yPos][coords.xPos] = "*";
	console.log(`${coords.yPos} ${coords.xPos}`);
	return res;
  }

  showSurface() {
	for (let i of this.mars2d) {
	  console.log(i.join(" "));
	}
	console.log("\n______________________\n");
  }

  action(...args) {
	this.buildGrid();

	let results = null;
	let [totalRows, totalCols] = [this.mars2d.length, this.mars2d[0].length];
	let [topPos, rightPos, bottomPos, leftPos] = [
	  0,
	  totalCols - 1,
	  totalRows - 1,
	  0
	];

	let coords = {
	  xPos: Math.floor(totalRows / 2),
	  yPos: Math.floor(totalCols / 2) - 1
	};

	let actionCommands = args.toString().toLowerCase();
	let actions = Array.from(actionCommands);
	let direction = this.direction;

	actions.some(action => {
	  let actionCount = 1;
	  results = 0;

	  action = action.toLowerCase() == "f" ? direction : action;

	  switch (action) {
		case "e":
		case "r":
		  results = this.resolveActions(actionCount, coords, () => coords.xPos, rightPos, () => coords.xPos++);
		  break;
		case "w":
		case "l":
		  results = this.resolveActions(actionCount, coords, () => coords.xPos, leftPos, () => coords.xPos--);
		  break;
		case "s":
		  results = this.resolveActions(actionCount, coords, () => coords.yPos, bottomPos, () => coords.yPos++);
		  break;

		case "n":
		  results = this.resolveActions(actionCount, coords, () => coords.yPos, topPos, () => coords.yPos--);
		  break;
		default:
		  console.log(`Please enter one of the following values: R | L | F`);
		  break;
	  }
	  if (!results) {
		this.mars2d[coords.yPos][coords.xPos] = "X";
		console.log(`${coords.yPos} ${coords.xPos} LOST`);
	  }
	  return !results;
	});

	this.showSurface();
  }
}

let robots = new RobotsOnMars(5, 3, 1, 1, "e");
robots.action("RFRFRFRF");
