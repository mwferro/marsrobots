# Mars Robots: Michael Ferro #

## To run:

* Ensure node is installed on the local system.
* Please note that this code is javascript es6
* From the command line, do:
```
node marsRobots.js
```
* Modify the command calls at will from the bottom of the script via the robots instantiation (calling action() like so):
```
robots.action("RFRFRFRF");
```
## Todo ##

* Ensure all possible position moves are accurate when given alternate start positions.
* Ensure that any other combinations of direction and forward commands give both the data sample output and expected output based on the above point.
* Make entries dynamically inputable via the command line.
* Implement tests.
